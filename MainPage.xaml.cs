﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace TimeLine
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private async void Entrar_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new InitialPage());
        }

        private async void Novo_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new SignInPage());
        }
    }
}
