﻿using Java.Util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeLine
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GamePage : ContentPage
    {
        private List<int> botoes;

        public GamePage()
        {
            InitializeComponent();
            botoes = new List<int>();
        }

        private void Button1_Clicked(object sender, EventArgs e)
        {
            Botao1.BackgroundColor = Color.DarkGray;
            botoes.Add(1);

            if (botoes.Count() == 3)
                ApertouTodos();

        }
        private void Button2_Clicked(object sender, EventArgs e)
        {
            Botao2.BackgroundColor = Color.DarkGray;
            botoes.Add(3);

            if(botoes.Count() == 3)
                ApertouTodos();

        }

        private void Button3_Clicked(object sender, EventArgs e)
        {            
            Botao3.BackgroundColor = Color.DarkGray;
            botoes.Add(2);

            if(botoes.Count() == 3)
                ApertouTodos();
        }

        private bool ApertouTodos()
        {
            Botao1.Text = "Big Bang: 13 bilhões de anos atrás";
            Botao2.Text = "Estréia Avengers Ultimato: 2019";
            Botao3.Text = "Bruna nasceu: 2000";

            int i = 1;
            bool acertou = true;
            foreach (int item in botoes)
            {
                if (item != i)
                {
                    acertou = false;
                    break;
                }
                i++;                
            }

            Continuar.IsVisible = true;

            if (acertou)
            {
                Botao1.BackgroundColor = Color.Green;
                Botao2.BackgroundColor = Color.Green;
                Botao3.BackgroundColor = Color.Green;
                return true;
            }
            else
            {
                Botao1.BackgroundColor = Color.Red;
                Botao2.BackgroundColor = Color.Red;
                Botao3.BackgroundColor = Color.Red;
                return false;
            }
        }

        private async void Continuar_Clicked(object sender, EventArgs e)
        {
            if (ApertouTodos())
                await Navigation.PushAsync(new GamePage2());
            else
                await Navigation.PushAsync(new GameOverPage());            
        }

        
    }
}    