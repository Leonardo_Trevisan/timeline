﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeLine
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GameOverPage : ContentPage
	{
		public GameOverPage ()
		{
			InitializeComponent ();
		}

        private async void Inicio_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new InitialPage());
        }
    }
}