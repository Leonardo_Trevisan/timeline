﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TimeLine
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InitialPage : ContentPage
	{
		public InitialPage ()
		{
			InitializeComponent ();
		}

        private async void Jogar_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new GamePage());
        }

        private async void Estatisticas_Clicked(object sender, EventArgs e)
        {
            await this.Navigation.PushAsync(new StatisticsPage());
        }
    }
}